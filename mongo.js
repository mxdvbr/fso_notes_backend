const mongoose = require('mongoose');
const { set, connect, Schema, model, connection } = mongoose;

if (process.argv.length < 3) {
    console.log('give password as argument')
    process.exit(1)
  }
  
const noteSchema = new mongoose.Schema({
  content: String,
  important: Boolean,
})

const Note = mongoose.model('Note', noteSchema)

const password = encodeURIComponent(process.argv[2])

const url =
  `mongodb+srv://fullstack:${password}@bortress.cayw6k9.mongodb.net/noteApp?retryWrites=true&w=majority&appName=bortress`

mongoose.set('strictQuery', false)
mongoose.connect(url).then(() => {
    // const noteSchema = new mongoose.Schema({
    //     content: String,
    //     important: Boolean,
    // })

    // const note = new Note({
    //     content: 'dogs are dumb',
    //     important: true,
    // })

    
    // note.save().then(result => {
    //     console.log('note saved!')
    //     mongoose.connection.close()
    // })

    Note.find({}).then(result => {
        result.forEach(note => {
        console.log(note)
        })
        mongoose.connection.close()
    })
})